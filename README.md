# incremental-checkstyle-maven

## requirements
- java21 (jdk)
- internet access - to fetch dependencies 

## techstack
- java21
- springboot 3.x

## building

```
./mvnw compile
```

## running tests

```
./mvnw test
```
